/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05 - readelf - EE 491 - Spr 2022
///
/// @file    elfcase.h
/// @version 1.0 - Initial version
///
/// elfcase - contains list of case statements for various cases elf header can have
///
/// @author   Duncan Lajousky <lajousky@hawaii.edu>
/// @date     03_22_2022
///////////////////////////////////////////////////////////////////////////////
#define FILENAME "elfcase.h"
//case statements for sections with multiple options//
//case statements for systems//
const char *sys_name(uint8_t sys) {
    switch (sys) {
    case 0x00: return "SYSTEM V";
    case 0x01: return "HP-UX";
    case 0x02: return "Linux";
    case 0x03: return "HP-UX";
    }
    return "Unknown";
}
//case statemetns for file type//
const char *type_name(uint16_t type) {
    switch (type) {
    case 0x00:   return "NONE";
    case 0x01:   return "REL";
    case 0x02:   return "EXEC";
    case 0x03:   return "DYN";
    case 0x04:   return "CORE";
    case 0xFE00: return "LOOS";
    case 0xFEFF: return "HIOS";
    case 0xFF00: return "LOPROC";
    case 0xFFFF: return "HIPROC";
    }
    return "Unknown";
}
//case statements for architecture//
const char *machine_name(uint16_t machine) {
    switch (machine) {
    case 0x01: return "x86";
    case 0x02: return "Intel MCU";
    case 0x03: return "MIPS";
    case 0x04: return "Intel 80960";
    case 0x05: return "PowerPC";
    case 0x06: return "PowerPC(64-bit)";
    case 0x07: return "ARM";
    case 0x3E: return "amd64";
    case 0x09: return "RISC-V";
    }
    return "Unknown";
}
