###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 05 - readelf - EE 491 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Create a C program with same funcionality as readelf -h
###
### @author  Duncan Lajousky <lajousky@hawaii.edu>
### @date    03_23_2022
###
###############################################################################
CC= gcc
CFLAGS= -g -Wall

TARGET= readelf

all: $(TARGET)

MemScan: readelf.c
				$(CC) $(CFLAGS) -o $(TARGET) MemScan.c

clean: rm -f $(TARGET)

