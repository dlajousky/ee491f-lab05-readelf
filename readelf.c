/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05 - readelf - EE 491 - Spr 2022
///
/// @file    readelf.c
/// @version 1.0 - Initial version
///
/// readelf - C program that has the same functionality of readelf -h
///
/// @author   Duncan Lajousky <lajousky@hawaii.edu>
/// @date     03_19__2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "elfstruc.h"
#include "elfcase.h"
#define READ(f, var) fread(&(var), sizeof(var), 1, (f))
void readElfHeader(FILE *fin, ElfHeader *elf);
void printElfHeader(const ElfHeader *elf);
const char *sys_name(uint8_t sys);
const char *type_name(uint16_t type);
const char *machine_name(uint16_t machine);
// argc for opening file// 
int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: %s FILENAME\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    FILE *fp = fopen(argv[1], "rb");
    if (!fp) {
        fprintf(stderr, "Error: cannot open input file\n");
        exit(EXIT_FAILURE);
    }
    ElfHeader elf;
    readElfHeader(fp, &elf);
    fclose(fp);
    printElfHeader(&elf);
    return 0;
}
//reading filepointers in structures and defining 32 vs 64 bit//
void readElfHeader(FILE *fp, ElfHeader *elf) {
    READ(fp, elf->ident);
    READ(fp, elf->type);
    READ(fp, elf->machine);
    READ(fp, elf->version);
    if (elf->ident[4] == 1) { // 32_bits
        READ(fp, elf->entry32);
        READ(fp, elf->phoff32);
        READ(fp, elf->shoff32);
    } else if (elf->ident[4] == 2) { // 64_bits
        READ(fp, elf->entry64);
        READ(fp, elf->phoff64);
        READ(fp, elf->shoff64);
    } else {
        fprintf(stderr, "Error: bad class: %d\n", elf->ident[4]);
        exit(EXIT_FAILURE);
    }
    READ(fp, elf->flags);
    READ(fp, elf->ehsize);
    READ(fp, elf->phentsize);
    READ(fp, elf->phnum);
    READ(fp, elf->shentsize);
    READ(fp, elf->shnum);
    READ(fp, elf->shstrndx);
}
//lines for parsing and printing each header section// 
void printElfHeader(const ElfHeader *elf) {
    printf("ELF Header:\n");
    printf("  Magic:  ");
    for (int i = 0; i < 16; i++) printf(" %02x", elf->ident[i]);
    printf("\n  %-34s %s\n", "Class:", elf->ident[4] == 1 ? "ELF32" : "ELF64");
    printf("  %-34s %s endian\n", "Data:", elf->ident[5] == 1 ? "little" : "endian");
    printf("  %-34s %u\n", "Version:", elf->ident[6]);
    printf("  %-34s %s\n", "OS:", sys_name(elf->ident[7]));
    printf("  %-34s %u\n", "ABI Version:", elf->ident[8]);
    printf("  %-34s %s\n", "Type:", type_name(elf->type));
    printf("  %-34s %s\n", "Machine:", machine_name(elf->machine));
    printf("  %-34s %x\n", "Version:", elf->version);
    if (elf->ident[4] == 1) { // 32_bits
        printf("  %-34s %x\n", "Entry point address:", elf->entry32);
        printf("  %-34s %u\n", "Start of program headers:", elf->phoff32);
        printf("  %-34s %u\n", "Start of section headers:", elf->shoff32);
    } else { // 64_bits
        printf("  %-34s %lx\n", "Entry point address:", elf->entry64);
        printf("  %-34s %lu\n", "Start of program headers:", elf->phoff64);
        printf("  %-34s %lu\n", "Start of section headers:", elf->shoff64);
    }
    printf("  %-34s %x\n", "Flags:", elf->flags);
    printf("  %-34s %u (bytes)\n", "Size of this header:", elf->ident[4] == 1 ? 32 : 64);
    printf("  %-34s %u\n", "Size of program headers:", elf->phentsize);
    printf("  %-34s %u\n", "Number of program headers:", elf->phnum);
    printf("  %-34s %u\n", "Size of section headers:", elf->shentsize);
    printf("  %-34s %u\n", "Number of section headers:", elf->shnum);
    printf("  %-34s %u\n", "Section header string table index:", elf->shstrndx);
}

