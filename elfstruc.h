/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05 - readelf - EE 491 - Spr 2022
///
/// @file    elfstruc.h
/// @version 1.0 - Initial version
///
/// elfstruct - headerfile to parse elf header into struct
///
/// @author   Duncan Lajousky <lajousky@hawaii.edu>
/// @date     03_20_2022
///////////////////////////////////////////////////////////////////////////////
#define FILENAME "elfstruct"
// creating the structure to parse and identify all the sections of the header//
typedef struct ElfHeader {
    uint8_t  ident[16];
    uint16_t type, machine;
    uint32_t version;
    union { uint32_t entry32; uint64_t entry64; };
    union { uint32_t phoff32; uint64_t phoff64; };
    union { uint32_t shoff32; uint64_t shoff64; };
    uint32_t flags;
    uint16_t ehsize, phentsize, phnum;
    uint16_t shentsize, shnum, shstrndx;
} ElfHeader;
